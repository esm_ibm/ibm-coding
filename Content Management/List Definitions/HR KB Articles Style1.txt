<?xml version="1.0" encoding="utf-8"?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">
<div id="hr_kb_articles" style="font-family: 'verdana' !important;">
<j:set var="jvar_num_rows" value="${current.getRowCount()}" />
<j:if test="${jvar_num_rows == 0}">
	<div style="margin-left: 30%;" >
		${gs.getMessage('No articles in this Knowledge Base')}
	</div>
</j:if>
<j:if test="${jvar_num_rows != 0}">
<g:for_each_record file="${current}" max="${jvar_max_entries}">

<j:set var="jvar_href" value="knowledge1.do?sysparm_document_key=kb_knowledge,${current.sys_id}" />

<div style="display:block;" class="kb-list applist">
<div class="kb-list article" style="width:98%; height:85px; margin-left:12px ; /*float:left;*/">
<br/>
	<span style="float:right; margin: -10px 100px 30px 30px;">
	
	<span class="views">
		<div style=" margin-bottom:-6px;" class="value">${current.sys_view_count}</div>
		<div>${gs.getMessage('views')}</div>
	</span>
	
	<j:if test="${current.use_count.getDisplayValue()=='0'}">
		<span class="useful-vote not-green">
			<div style=" margin-bottom:-6px;" class="value">${current.use_count}</div>
			<div>${gs.getMessage('votes')}</div>
		</span>

	</j:if>
	<j:if test="${current.use_count.getDisplayValue()!='0'}">
		<span class="useful-vote green">
			<div class="value">${current.use_count}</div>
			<div>${gs.getMessage('votes')}</div>
		</span>
	</j:if>

	</span>

<!--	
	<span class="rating" style="float:right;">
		<div><img src="transparent.gifx" height="12px" class="star rating-${current.rating}"/></div>

	</span>
-->
	
	<span style="font-size:1.8; font-family:; color:#000;">
	
		<h2 class="short-description" style="border:0; margin-top:-15px;">
		<a style="font-size:1.0rem; padding-left: 20px; font-family: Verdana; color: #b9204b; font-weight: 300;" href="${jvar_href}">${current.short_description.getDisplayValue()}</a>
		</h2>
	</span>
	<span class="kb-updated-stamp" style="font-size:1.2; color:#888; padding-left: 20px; ">
		<b>${current.kb_category.getDisplayValue()}</b>
		${AMP}#8226 ${gs.getMessage('by')} ${current.author.getDisplayValue()}
		<j:if test="${gs.getProperty('glide.knowman.search.show_last_modified') == 'true'}">
			<g:evaluate var="jvar_modified_time_ago">
			 new GlideTimeAgo().format(current.getElement('sys_updated_on').getGlideObject())
			</g:evaluate>
			${AMP}#8226 ${gs.getMessage('last modified')} ${jvar_modified_time_ago}
		</j:if>
		<j:if test="${gs.getProperty('glide.knowman.search.show_published') == 'true'}">
			<g:evaluate var="jvar_published_time_ago">
			 new GlideTimeAgo().format(current.getElement('published').getGlideObject())
			</g:evaluate>
			${AMP}#8226 ${gs.getMessage('published')} ${jvar_published_time_ago}
		</j:if>
	</span>
	<br/>
	<span style="font-size: 12px; color:#999;">
	${current.description.getDisplayValue()}
	</span>
<div style="clear:both;"/>
<br/>
</div>
</div>
</g:for_each_record>
</j:if>
</div>
</j:jelly>