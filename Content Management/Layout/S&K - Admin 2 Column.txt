<?xml version="1.0" encoding="utf-8"?>
<j:jelly trim="true" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">
<g:requires name="ess.portal.globals.jsdbx" />
<div class="layout_container">

	<div id="${jvar_name}" class="site_layout">	
		<div class="content_header_container" id="dropzone0" dropzone="true" />
		<div class="content_body_container_2_column" style="margin-right: auto; margin-left: auto;">
			<div id="dropzone10" dropzone="true" class="col-xs-6 col-sm-8 col-md-9 col-lg-9" />
			<div id="dropzone20" dropzone="true" class="col-xs-6 col-sm-4 col-md-3 col-lg-3" />
			
                        <div style="clear:both;" />
		</div>
		<div class="content_footer_container" id="dropzone100" dropzone="true" />
		<div class="content_footer_container" id="dropzone200" dropzone="true" />
	</div>
	
		
</div>

</j:jelly>