
========================================
Name:
S&K - Life Events page

Type:
HR KB Articles Style

Title:
S&K - Life Events page

Maximum entries:
5

Table:
Knowledge [kb_knowledge]

Order:
Published

Order direction:
Descending

Query:
	Active	-	is	-	true
		AND
	Knowledge base	-	is	-	Human Resources Knowledge
		AND
	Workflow	-	is	-	Published
		AND
	Category(kb_category)	-	is	-	Life Events

========================================
