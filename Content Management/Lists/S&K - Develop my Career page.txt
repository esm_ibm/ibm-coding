
========================================
Name:
S&K - Develop my Career page

Type:
HR KB Articles Style

Title:
S&K - Develop my Career page

Maximum entries:
5

Table:
Knowledge [kb_knowledge]

Order:
Published

Order direction:
Descending

Query:
	Active	-	is	-	true
		AND
	Knowledge base	-	is	-	Human Resources Knowledge
		AND
	Workflow	-	is	-	Published
		AND
	Category(kb_category)	-	is	-	Develop my Career

========================================
