<?xml version="1.0" encoding="utf-8"?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">


<style>
/************************************************
Dan's thumb float grid styles
************************************************/
DIV.thumbnail_container{
	margin-bottom:12px; 
	margin-right:12px; 
	width:320px; 
	border:1px solid #ccc; 
	padding:10px; 
	float:left; 
	text-align:center;
}

#dropzone10 DIV.thumbnail_container{
	background-color:white;
	border:1px solid black; 
}

#dropzone20 DIV.thumbnail_container{
	background-color:blue;
	border:1px solid black; 
}

#dropzone100 DIV.thumbnail_container{
	background-color:green;
	border:1px solid lime; 
}

IMG.thumb.screen {
	/*width: 40%;*/
	max-width: 150px;
}

SPAN.image_caption{
	font-size:smaller;
	font-weight:normal;
	color:#666;
}
SPAN.image_caption. SPAN.caption_name{
	font-size:larger;
	font-weight:bold;
	color:#444;
}
</style>
<div class="content_list_title">${jvar_title}</div>
    <g:for_each_record file="${current}" max="${jvar_max_entries}">


        <div class="thumbnail_container popup_form_actions">
<!--
                <a class="" href="${current.name}x" target="_blank"><img src="${current.name}x" class="screen thumb"/></a><br/>
-->
                ${current.short_description}<br/>
        	<a href="knowledge.do?${AMP}sysparm_document_key=kb_knowledge,${current.sys_id}" target="_top" >
        	<img src="${current.u_thumbnail.getDisplayValue()}" alt="${current.short_description}" height="210px"/></a><br/>

                <span class="image_caption">
<!---->
                <span class="caption_name">${current.name}</span>
                ${current.height}
                ${current.width}

                </span>

<!---->
                ${current.short_description}<br/>
        	<a href="knowledge.do?${AMP}sysparm_document_key=kb_knowledge,${current.sys_id}" target="_top" >
        	<img src="${current.u_thumbnail.getDisplayValue()}" alt="${current.short_description}" height="210px"/></a><br/>

        </div>

    </g:for_each_record>
<div style="clear:both;"/>
</j:jelly>