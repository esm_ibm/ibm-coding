<?xml version="1.0" encoding="utf-8" ?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">

	<j:set var="jvar_general_hr_request_url" value="com.glideapp.servicecatalog_cat_item_view.do?v=1&amp;sysparm_id=b1688fb8ef22010047920fa3f8225631&amp;sysparm_link_parent=11371d040f868a00b0d34bfce1050e94&amp;sysparm_catalog=bf1a3dd0ef0311004c7236caa5c0fbae&amp;sysparm_catalog_view=catalog_hr_catalog"/>
	
	<div style="overflow:scroll !important; -webkit-overflow-scrolling:touch !important;">
		<iframe src="${jvar_general_hr_request_url}" target="_self" style="width: 100%; height: 650px;">
		</iframe>
	</div>
	
</j:jelly>