
==========
[Condition]:
----------
gs.getSession().hasRole(gs.getProperty('glide.ui.can_search'));
==========

==========
[Dynamic content]:
----------
<?xml version="1.0" encoding="utf-8"?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">

	<g:evaluate var="sysparm_search"> 
                var arr = new Object();
                var sysparm_search = RP.getParameterValue('sysparm_search');
                RP.setView("text_search")
                sysparm_search;
	</g:evaluate>
        <j:set var="jvar_cms_search_result" value="true" />
        <j:set var="jvar_ts_groupid" value="" />
        <j2:set var="sysparm_query" value="123TEXTQUERY321=${sysparm_search}" />
        <g:inline template="textsearch.xml"/>
</j:jelly>
==========
