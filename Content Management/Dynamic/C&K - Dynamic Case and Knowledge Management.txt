<?xml version="1.0" encoding="utf-8" ?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">
	
	<style>
		#case_and_knowledge {width: 100%; margin-top: 25px; margin-bottom: 25px;}
		#case_and_knowledge .outer_cell {float: left; width: 33%; padding-left: 30px; padding-right: 30px;}
		#case_and_knowledge .cell {position: relative; background: url('native_notification_icon.png'); height: 200px; border-radius: 8px; border: 5px outset #008B5E;}
		#case_and_knowledge .cell:hover {border-radius: 15px; border: 6px inset #008B5E;}
		#case_and_knowledge .cell h3 {position: absolute; bottom: -9px; text-align: center !important; color: white; background-color: #4D4D4D; opacity: 0.7; width: 100%; height: 20%; z-index: 1; font-weight: bold; font-size: 18px;}
		#case_and_knowledge .cell p {position: absolute; top: 0px; visibility: hidden; text-align: center !important; padding: 10px; color: white; background-color: #1A1A1A; opacity: 0.85; font-weight: bold; width: 100%; font-size: 13px !important;}
		#case_and_knowledge .cell:hover p {visibility: visible; font-size: 1.2em; text-overflow: ellipsis; overflow: hidden; height: 80%;}
		#case_and_knowledge .cell:hover p:empty {display: none;}
		#case_and_knowledge .cell h3, #case_and_knowledge .cell p {font-family: 'verdana' !important;}
	</style>
	
	<div id="case_and_knowledge">
		
		<div class="outer_cell">
			<a href="case_management.do">
				<div class="cell" style="background: url('hr.png') no-repeat; background-position: center;">
					<h3>My HR Cases</h3>
					<p>Browse the HR Case Records</p>
				</div>
			</a>
		</div>
		
		<div class="outer_cell">
			<a href="ask_hr_question.do">
				<div class="cell" style="background: url('doc-policy-ask.png') no-repeat; background-position: center;">
					<h3>Ask HR a Question</h3>
					<p>Browse the Knowledge Query Raising Catalog.  You can ask a question about: 'Employee Changes', 'Employee Policy Queries' or 'General Questions'</p>
				</div>
			</a>
		</div>
		
		<div class="outer_cell" style="visibility: ${jvar_display}">
			<a href="knowledge_management.do">
				<div class="cell" style="background: url('knowledge-base.png') no-repeat; background-position: center;">
					<h3>Knowledge Management</h3>
					<p>Browse the Knowledge Base of Human Resources</p>
				</div>
			</a>
		</div>
		
	</div>
	
</j:jelly>