<?xml version="1.0" encoding="utf-8" ?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">

	<g:evaluate var="jvar_roles_1" jelly="true">
		var isValid = gs.hasRole(${current.u_list_test_1});
		isValid;
	</g:evaluate>
	<g:set_if var="jvar_display1" test="${jvar_roles_1}" true="visible" false="hidden" />
	
	<h1>
		Value: ${jvar_display1}
	</h1>
	
</j:jelly>