<?xml version="1.0" encoding="utf-8" ?>
<j:jelly trim="false" xmlns:j="jelly:core" xmlns:g="glide" xmlns:j2="null" xmlns:g2="null">
	
<style>
	#ios_dropdown {text-align: center;}
	#ios_dropdown ul {list-style: none; line-height: 50px; padding: 0px;}
	#ios_dropdown li {background-color: #f6f6f6; height: 50px;}
	#ios_dropdown li ul {visibility: hidden; display: none; position: relative; width: 100%; top: 0px; left: 0px; padding: 0px; z-index: 10;}
	#ios_dropdown li ul li {}
	#ios_dropdown li:hover ul {visibility: visible; display: block;}
	#ios_dropdown li:hover {background-color: lightgrey;}
	#ios_dropdown li:hover ul li:hover {background-color: lightgrey;}
	#ios_dropdown a {text-decoration: none; color: black;}
</style>

<div id="ios_dropdown">
	<ul>
		<li>
			${SNC.GlideHTMLSanitizer.sanitize(gs.getUserDisplayName())}
			<ul>
				<a href="home1.do"><li>my Home</li></a>
				<a href="case_management.do"><li>my Open Cases</li></a>
				<a href="myteam_homepage.do"><li>my Team</li></a>
				<li role="separator" class="divider"></li>
				<a onclick="logoutPage.submit()"><li>${gs.getMessage('Logout')}</li></a>
			</ul>
		</li>
	</ul>
</div>
	
	<g:evaluate>
		var loginAction = "login.do";
		var logoutAction = "logout.do";
		var tran = GlideTransaction.get();
		if (tran.isVirtual()) {
			loginAction = "../" + loginAction;
			logoutAction = "../" + logoutAction;
		}
		var pageLink = new GlideCMSPageLink().getPageLink(current_page.getID()).substring(3);
		var documentKey = RP.getParameterValue('sysparm_document_key');
		if (documentKey)
			pageLink += '?sysparm_document_key=' + documentKey;
		pageLink = GlideStringUtil.urlEncode(pageLink);
	</g:evaluate>
	
	<j:if test="${!gs.getSession().isLoggedIn()}">
		<div class="content_list_title">${gs.getMessage('Login')}</div>  
		<form name="loginPage" id="loginPage" action="${loginAction}" method="post">
			<g:inline template="output_messages.xml" />
			<table>
				<input type="hidden" name="sys_action" value="sysverb_login" />
				<input type="hidden" name="sysparm_goto_url" value="logout_redirect.do?sysparm_url=${pageLink}" />
				<input type="hidden" name="sysparm_login_url" value="logout_redirect.do?sysparm_url=${pageLink}" />
				<tr>
					<td><label for="user_name">${gs.getMessage('User name')}:</label></td>
				</tr>
				<tr>
					<td><input type="text" id="user_name" name="user_name" value="" autocomplete="off"/></td>
				</tr>
				<tr>
					<td><label for="user_password">${gs.getMessage('Password')}:</label></td>			
				</tr>
				<tr>
					<td><input type="password" id="user_password" name="user_password"/></td>
				</tr>
				
				<j:if test="${gs.getProperty('glide.ui.forgetme') != 'true'}">
				<tr id="remembermeoption" style="display: notnone">
					<td>
						<script eval="true"> function setCheckBox(e) { if (e.checked) { e.value = "true"; } else { e.value = "false"; } } </script>
						<input type="checkbox" id="remember_me" name="remember_me" value="true" checked="true" onClick="setCheckBox(this);"/>
						<label for="remember_me">${gs.getMessage('Remember me')}</label>
					</td>			
				</tr>
				<tr>
					<td>
						<button class="content_login" type="submit" > ${gs.getMessage('Login')} </button>
					</td>
				</tr>
				</j:if>
			</table>
		</form>
	</j:if>
	<j:if test="${gs.getSession().isLoggedIn()}">
		<form name="logoutPage" action="${logoutAction}">
			
		</form>
	</j:if>

</j:jelly>